import React from "react";

class ReadString extends React.Component {
	state = { dataKey: null };

	/**
	 * Quick Recap
	 *
	 * The most important thing to get out of this section here is that there are two steps to reading a value with Drizzle:
	 *
	 * First, you need to let Drizzle know what variable you want to watch for. Drizzle will give you a dataKey in return and you need to save it for later reference.
	 * Second, due to the asynchronous nature of how Drizzle works, you should be watching for changes in drizzleState. Once the variable accessed by the dataKey exists, you will be able to get the value you are interested in.
	 */
	componentDidMount() {
		const { drizzle } = this.props;

		/** When the component mounts, we first grab a reference to the contract we are interested in and assign it to contract. */
		const contract = drizzle.contracts.MyStringStore;

		/** Inspect drizzle and drizzleState in console log. */
		console.log({'drizzle': this.props.drizzle, 'drizzleState': this.props.drizzleState});

		// let drizzle know we want to watch the `myString` method
		/**
		 * We then need to tell Drizzle to keep track of a variable we are interested in. In order to do that, we call the .cacheCall() function on the myString getter method.
		 * What we get in return is a dataKey that allows us to reference this variable. We save this to the component's state so we can use it later.
		 */
		const dataKey = contract.methods["myString"].cacheCall();

		// save the `dataKey` to local component state for later reference
		this.setState({ dataKey });
	}

	render() {
		// get the contract state from drizzleState
		/** From the drizzleState, we grab the slice of the state we are interested in, which in this case is the MyStringStore contract. From there, we use the dataKey we saved from before in order to access the myString variable. */
		const { MyStringStore } = this.props.drizzleState.contracts;

		// using the saved `dataKey`, get the variable we're interested in
		const myString = MyStringStore.myString[this.state.dataKey];

		// if it exists, then we display its value
		/** Finally, we write myString && myString.value to show the value of the variable if it exists, or nothing otherwise. And in this case, it should show "Hello World" since that is the string the contract is initialized with. */
		return <p>My stored string: {myString && myString.value}</p>;
	}
}

export default ReadString;
